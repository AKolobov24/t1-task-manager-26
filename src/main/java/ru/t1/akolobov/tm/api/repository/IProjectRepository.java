package ru.t1.akolobov.tm.api.repository;

import ru.t1.akolobov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
